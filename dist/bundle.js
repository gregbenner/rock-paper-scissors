(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"./app/scripts/main.js":[function(require,module,exports){
var initialize = require('./initialize.js'),
    battleLogic = require('./battleLogic.js'),
    updateScore = require('./updateScore.js'),
    utils = require('./utils/generic.js'),
    rock = require('./rock.js'),
    paper = require('./paper.js'),
    scissors = require('./scissors.js'),
    storage;

var container = document.getElementById('site'),
    userName = container.getElementsByClassName('userName')[0],
    scoreBoard = container.getElementsByClassName('scoreboard')[0];

if(initialize.checkStorage()) {
   storage = require('./utils/storage.js');
}

initialize.setPage();
initialize.setNames();
updateScore.drawScore();

var weaponChoice = container.getElementsByClassName('weaponChoice');

 var handleWeapon = function() {
    var attribute = this.getAttribute("data-value");

    switch(attribute) {
      case 'rock' :
        rock.setWeapon('playerA');
        weaponValue = rock.weaponValue;
        break;
      case 'scissors' :
        scissors.setWeapon('playerA');
        weaponValue = scissors.weaponValue;
        break;
      case 'paper' :
        paper.setWeapon('playerA');
        weaponValue = paper.weaponValue;
        break;
    }
    battleLogic.getWinner(weaponValue);
};

for(var i=0;i<weaponChoice.length;i++){
  weaponChoice[i].addEventListener('click', handleWeapon, false);
}

initialize.gatherUserNames();
initialize.resetButton();


},{"./battleLogic.js":"/Users/greg/Sites3/eBay/app/scripts/battleLogic.js","./initialize.js":"/Users/greg/Sites3/eBay/app/scripts/initialize.js","./paper.js":"/Users/greg/Sites3/eBay/app/scripts/paper.js","./rock.js":"/Users/greg/Sites3/eBay/app/scripts/rock.js","./scissors.js":"/Users/greg/Sites3/eBay/app/scripts/scissors.js","./updateScore.js":"/Users/greg/Sites3/eBay/app/scripts/updateScore.js","./utils/generic.js":"/Users/greg/Sites3/eBay/app/scripts/utils/generic.js","./utils/storage.js":"/Users/greg/Sites3/eBay/app/scripts/utils/storage.js"}],"/Users/greg/Sites3/eBay/app/scripts/battleLogic.js":[function(require,module,exports){
var updateScore = require('./updateScore.js');
var rock = require('./rock.js');
var paper = require('./paper.js');
var scissors = require('./scissors.js');

var battleLogic = {};


battleLogic.updatePlayerScores = function(winner) {
  if(winner === 'A') {
    updateScore.newScore('playerA');
    updateScore.showLoser('A');
  } else if (winner === 'B') {
    updateScore.newScore('playerB');
    updateScore.showLoser('B');
  } else {
    // draw
  }

}

battleLogic.computerChoice = function() {
  var random = Math.floor(Math.random() * 3);
  if(random === 0) {
    rock.setWeapon('playerB');
  } else if (random === 1) {
    scissors.setWeapon('playerB');
  } else {
    paper.setWeapon('playerB');
  }
  return random;
};

battleLogic.getWinner = function(player1) {
  player2 =  this.computerChoice();
  if (player1 === player2) return 'draw'
  // 0 = rock, 1 = scissor, 2 = paper
  if(player1 === 0) {
    if(player2 === 1) {
      return this.updatePlayerScores('A');
    } else {
      return this.updatePlayerScores('B');
    }
  }

  if(player1 === 1) {
    if(player2 === 0) {
      return this.updatePlayerScores('B');
    } else {
      return this.updatePlayerScores('A');
    }
  }

  if(player1 === 2) {
    if(player2 === 0) {
      return this.updatePlayerScores('A');
    } else {
     return this.updatePlayerScores('B');
    }
  } 
};


module.exports = battleLogic;
},{"./paper.js":"/Users/greg/Sites3/eBay/app/scripts/paper.js","./rock.js":"/Users/greg/Sites3/eBay/app/scripts/rock.js","./scissors.js":"/Users/greg/Sites3/eBay/app/scripts/scissors.js","./updateScore.js":"/Users/greg/Sites3/eBay/app/scripts/updateScore.js"}],"/Users/greg/Sites3/eBay/app/scripts/initialize.js":[function(require,module,exports){
var utils = require('./utils/generic.js');
var updateScore = require('./updateScore.js');
var container = document.getElementById('site'),
    userName = container.getElementsByClassName('userName')[0],
    storage = require('./utils/storage.js'),
    gameboard = container.getElementsByClassName('gameboard')[0],
    playerAzone = gameboard.getElementsByClassName('playerAzone')[0],
    playerBzone = gameboard.getElementsByClassName('playerBzone')[0],
    playerAname = playerAzone.querySelectorAll('.name span')[0],
    playerBname = playerBzone.querySelectorAll('.name span')[0],
    page1 = document.getElementById('page1'),
    page2 = document.getElementById('page2'),
    pages = document.getElementsByClassName('page');


initScripts = {
  setPage: function() {
    
    for(var i=0;i<pages.length;i++) {
      utils.removeClass(pages[i], 'show');
    }
    var currentPage  = localStorage.getItem('rps-currentPage') || 'page1';
    pageElement  = document.getElementById(currentPage);
    utils.addClass(pageElement, 'show');

  },


  checkStorage: function() {
     if ('localStorage' in window && window['localStorage'] !== null) {
        return true;
    } else {
        alert('Cannot store user preferences as your browser do not support local storage');
    }
  },
  storeNames : function(playerNames) {
    storage.localStorage.setItem('rps-playerA', playerNames.playerA);
    storage.localStorage.setItem('rps-playerB', playerNames.playerB);
    this.setNames();
  },
  setNames : function() {
    playerAname.textContent = localStorage.getItem('rps-playerA') || 'unknown';
    playerBname.textContent = localStorage.getItem('rps-playerB') || 'unknown';
    this.nextPage();
  },
  gatherUserNames: function() {
      var playerNames = {};
      var _this = this;
      userName.addEventListener("submit", function(event){
      var formLength = userName.elements.length;
      
      for (var i = 0; i < formLength; i++) {
        playerNames[userName.elements[i].name] = userName.elements[i].value;
      };
      _this.storeNames(playerNames);
      event.preventDefault();
    }, false);
      
  },

  nextPage: function() {
    utils.removeClass(page1, 'show');
    setTimeout(function() {
        utils.addClass(page2, 'show');
     } , 550);

    localStorage.setItem('rps-currentPage', 'page2');
    
  },

  resetButton : function() {
    var resetBtn = document.querySelectorAll('.reset')[0];
    resetBtn.addEventListener('click', function() {
      updateScore.resetGame();
      updateScore.drawScore();
    });
  }
  
}
module.exports = initScripts;
},{"./updateScore.js":"/Users/greg/Sites3/eBay/app/scripts/updateScore.js","./utils/generic.js":"/Users/greg/Sites3/eBay/app/scripts/utils/generic.js","./utils/storage.js":"/Users/greg/Sites3/eBay/app/scripts/utils/storage.js"}],"/Users/greg/Sites3/eBay/app/scripts/paper.js":[function(require,module,exports){
var Weapon = require('./weapon.js');
var Paper = Object.create(Weapon);

Paper.type = 'paper';

module.exports = Paper;
},{"./weapon.js":"/Users/greg/Sites3/eBay/app/scripts/weapon.js"}],"/Users/greg/Sites3/eBay/app/scripts/rock.js":[function(require,module,exports){
var Weapon = require('./weapon.js');
var Rock = Object.create(Weapon);

// Rock.__proto__ = Weapon;
Rock.type = 'rock';

module.exports = Rock;
},{"./weapon.js":"/Users/greg/Sites3/eBay/app/scripts/weapon.js"}],"/Users/greg/Sites3/eBay/app/scripts/scissors.js":[function(require,module,exports){
var Weapon = require('./weapon.js');
var Scissors = Object.create(Weapon);

Scissors.type = 'scissors';

module.exports = Scissors;
},{"./weapon.js":"/Users/greg/Sites3/eBay/app/scripts/weapon.js"}],"/Users/greg/Sites3/eBay/app/scripts/updateScore.js":[function(require,module,exports){
var storage = require('./utils/storage.js'),
    utils = require('./utils/generic.js'),
    updateScore = {},
    playerA_score_zone  = document.querySelectorAll('.playerAzone .score')[0],
    playerB_score_zone =  document.querySelectorAll('.playerBzone .score')[0],
    playerAscore = storage.localStorage.getItem('rps-playerAscore') || 0,
    playerBscore = storage.localStorage.getItem('rps-playerBscore') || 0,
    pages = document.getElementsByClassName('page'),
    weapons = document.querySelectorAll('.weapon'),
    playerAWeapon = document.getElementById('weaponA'),
    playerBWeapon = document.getElementById('weaponB');
    

var initialize = require('./initialize.js');

updateScore.drawScore = function() {
  playerA_score_zone.textContent = playerAscore;
  playerB_score_zone.textContent = playerBscore;
};

updateScore.resetWeapons = function() {
  for (var i = 0; i < weapons.length; i++) {
    utils.newClass(weapons[i], 'weapon')
  }
};

updateScore.newScore = function(player) {
  if(player === 'playerA') {
    var playerAscore = storage.localStorage.getItem('rps-playerAscore');
    playerAscore++;
    storage.localStorage.setItem('rps-playerAscore', playerAscore);
    playerA_score_zone.textContent = playerAscore;
  } else if (player === 'playerB') {
    var playerBscore = storage.localStorage.getItem('rps-playerBscore');
    playerBscore++;
    storage.localStorage.setItem('rps-playerBscore', playerBscore);
    playerB_score_zone.textContent = playerBscore;
  }
};

updateScore.showLoser = function(player) {
  for (var i = 0; i < weapons.length; i++) {
       utils.removeClass(weapons[i], 'grayscale');
   };

   setTimeout(function() { 
    if(player === 'A') {
      utils.addClass(playerBWeapon, 'grayscale');
    } else if (player === 'B') {
      utils.addClass(playerAWeapon, 'grayscale');
    }
  }, 300)
  
};

updateScore.setPage = function() {
    
    for(var i=0;i<pages.length;i++){
      utils.removeClass(pages[i], 'show');
    }
    var currentPage  = localStorage.getItem('rps-currentPage') || 'page1';
    pageElement  = document.getElementById(currentPage);
    utils.addClass(pageElement, 'show');
  }

updateScore.resetGame = function() {
  console.log('reset')
  storage.localStorage.setItem('rps-playerAscore', 0);
  storage.localStorage.setItem('rps-playerBscore', 0);
  localStorage.setItem('rps-currentPage', 'page1');
  this.drawScore();
  this.resetWeapons();
  this.setPage();
};

module.exports = updateScore;
},{"./initialize.js":"/Users/greg/Sites3/eBay/app/scripts/initialize.js","./utils/generic.js":"/Users/greg/Sites3/eBay/app/scripts/utils/generic.js","./utils/storage.js":"/Users/greg/Sites3/eBay/app/scripts/utils/storage.js"}],"/Users/greg/Sites3/eBay/app/scripts/utils/generic.js":[function(require,module,exports){
var utils = {};

utils.toggleClass = function(el, className) {
  if (el.classList) {
    el.classList.toggle(className);
  } else {
  var classes = el.className.split(' ');
  var existingIndex = classes.indexOf(className);

  if (existingIndex >= 0)
    classes.splice(existingIndex, 1);
  else
    classes.push(className);

  el.className = classes.join(' ');
  }
};

utils.newClass = function(el, className) {
  el.className = className;
};

utils.addClass = function(el, className) {
  if (el.classList)
  el.classList.add(className);
  else
    el.className += ' ' + className;
};

utils.removeClass = function(el, className) {
  if (el.classList)
  el.classList.remove(className);
else
  el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
};

utils.fadeInShow = function(el) {
  el.style.opacity = 0;
  var last = +new Date();
  var tick = function() {
    el.style.opacity = +el.style.opacity + (new Date() - last) / 400;
    last = +new Date();
    if (+el.style.opacity < 1) {
      (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16)
    }
  };

  tick();
};



module.exports = utils;
},{}],"/Users/greg/Sites3/eBay/app/scripts/utils/storage.js":[function(require,module,exports){
var storage = {
  localStorage : window.localStorage,
  sessionStorage :  window.sessionStorage
}
module.exports = storage;
},{}],"/Users/greg/Sites3/eBay/app/scripts/weapon.js":[function(require,module,exports){
var utils = require('./utils/generic.js');
var container = document.getElementById('site');
var userName = container.getElementsByClassName('userName')[0];


var Weapon = {};

Weapon.setValue = function() {
    var type = this.type;
    switch (type) {
        case 'rock' :
          this.weaponValue = 0
          break;
        case 'scissors' :
          this.weaponValue = 1
          break;
        case 'paper' :
          this.weaponValue = 2
          break;
    }
};

Weapon.setWeapon = function(player) {
  this.player = player;
  var playerZone = this.player + 'zone';
  var playerEl = container.getElementsByClassName(playerZone)[0];
  var playerWeaponEl = playerEl.getElementsByClassName('weapon')[0];

  utils.newClass(playerWeaponEl, 'weapon ' + this.type);
  this.setValue();
}

module.exports = Weapon;
},{"./utils/generic.js":"/Users/greg/Sites3/eBay/app/scripts/utils/generic.js"}]},{},["./app/scripts/main.js"])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJhcHAvc2NyaXB0cy9tYWluLmpzIiwiYXBwL3NjcmlwdHMvYmF0dGxlTG9naWMuanMiLCJhcHAvc2NyaXB0cy9pbml0aWFsaXplLmpzIiwiYXBwL3NjcmlwdHMvcGFwZXIuanMiLCJhcHAvc2NyaXB0cy9yb2NrLmpzIiwiYXBwL3NjcmlwdHMvc2Npc3NvcnMuanMiLCJhcHAvc2NyaXB0cy91cGRhdGVTY29yZS5qcyIsImFwcC9zY3JpcHRzL3V0aWxzL2dlbmVyaWMuanMiLCJhcHAvc2NyaXB0cy91dGlscy9zdG9yYWdlLmpzIiwiYXBwL3NjcmlwdHMvd2VhcG9uLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2xEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMvREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMvRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ0xBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ05BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNMQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMzRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNwREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwidmFyIGluaXRpYWxpemUgPSByZXF1aXJlKCcuL2luaXRpYWxpemUuanMnKSxcbiAgICBiYXR0bGVMb2dpYyA9IHJlcXVpcmUoJy4vYmF0dGxlTG9naWMuanMnKSxcbiAgICB1cGRhdGVTY29yZSA9IHJlcXVpcmUoJy4vdXBkYXRlU2NvcmUuanMnKSxcbiAgICB1dGlscyA9IHJlcXVpcmUoJy4vdXRpbHMvZ2VuZXJpYy5qcycpLFxuICAgIHJvY2sgPSByZXF1aXJlKCcuL3JvY2suanMnKSxcbiAgICBwYXBlciA9IHJlcXVpcmUoJy4vcGFwZXIuanMnKSxcbiAgICBzY2lzc29ycyA9IHJlcXVpcmUoJy4vc2Npc3NvcnMuanMnKSxcbiAgICBzdG9yYWdlO1xuXG52YXIgY29udGFpbmVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3NpdGUnKSxcbiAgICB1c2VyTmFtZSA9IGNvbnRhaW5lci5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCd1c2VyTmFtZScpWzBdLFxuICAgIHNjb3JlQm9hcmQgPSBjb250YWluZXIuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnc2NvcmVib2FyZCcpWzBdO1xuXG5pZihpbml0aWFsaXplLmNoZWNrU3RvcmFnZSgpKSB7XG4gICBzdG9yYWdlID0gcmVxdWlyZSgnLi91dGlscy9zdG9yYWdlLmpzJyk7XG59XG5cbmluaXRpYWxpemUuc2V0UGFnZSgpO1xuaW5pdGlhbGl6ZS5zZXROYW1lcygpO1xudXBkYXRlU2NvcmUuZHJhd1Njb3JlKCk7XG5cbnZhciB3ZWFwb25DaG9pY2UgPSBjb250YWluZXIuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnd2VhcG9uQ2hvaWNlJyk7XG5cbiB2YXIgaGFuZGxlV2VhcG9uID0gZnVuY3Rpb24oKSB7XG4gICAgdmFyIGF0dHJpYnV0ZSA9IHRoaXMuZ2V0QXR0cmlidXRlKFwiZGF0YS12YWx1ZVwiKTtcblxuICAgIHN3aXRjaChhdHRyaWJ1dGUpIHtcbiAgICAgIGNhc2UgJ3JvY2snIDpcbiAgICAgICAgcm9jay5zZXRXZWFwb24oJ3BsYXllckEnKTtcbiAgICAgICAgd2VhcG9uVmFsdWUgPSByb2NrLndlYXBvblZhbHVlO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ3NjaXNzb3JzJyA6XG4gICAgICAgIHNjaXNzb3JzLnNldFdlYXBvbigncGxheWVyQScpO1xuICAgICAgICB3ZWFwb25WYWx1ZSA9IHNjaXNzb3JzLndlYXBvblZhbHVlO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ3BhcGVyJyA6XG4gICAgICAgIHBhcGVyLnNldFdlYXBvbigncGxheWVyQScpO1xuICAgICAgICB3ZWFwb25WYWx1ZSA9IHBhcGVyLndlYXBvblZhbHVlO1xuICAgICAgICBicmVhaztcbiAgICB9XG4gICAgYmF0dGxlTG9naWMuZ2V0V2lubmVyKHdlYXBvblZhbHVlKTtcbn07XG5cbmZvcih2YXIgaT0wO2k8d2VhcG9uQ2hvaWNlLmxlbmd0aDtpKyspe1xuICB3ZWFwb25DaG9pY2VbaV0uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBoYW5kbGVXZWFwb24sIGZhbHNlKTtcbn1cblxuaW5pdGlhbGl6ZS5nYXRoZXJVc2VyTmFtZXMoKTtcbmluaXRpYWxpemUucmVzZXRCdXR0b24oKTtcblxuIiwidmFyIHVwZGF0ZVNjb3JlID0gcmVxdWlyZSgnLi91cGRhdGVTY29yZS5qcycpO1xudmFyIHJvY2sgPSByZXF1aXJlKCcuL3JvY2suanMnKTtcbnZhciBwYXBlciA9IHJlcXVpcmUoJy4vcGFwZXIuanMnKTtcbnZhciBzY2lzc29ycyA9IHJlcXVpcmUoJy4vc2Npc3NvcnMuanMnKTtcblxudmFyIGJhdHRsZUxvZ2ljID0ge307XG5cblxuYmF0dGxlTG9naWMudXBkYXRlUGxheWVyU2NvcmVzID0gZnVuY3Rpb24od2lubmVyKSB7XG4gIGlmKHdpbm5lciA9PT0gJ0EnKSB7XG4gICAgdXBkYXRlU2NvcmUubmV3U2NvcmUoJ3BsYXllckEnKTtcbiAgICB1cGRhdGVTY29yZS5zaG93TG9zZXIoJ0EnKTtcbiAgfSBlbHNlIGlmICh3aW5uZXIgPT09ICdCJykge1xuICAgIHVwZGF0ZVNjb3JlLm5ld1Njb3JlKCdwbGF5ZXJCJyk7XG4gICAgdXBkYXRlU2NvcmUuc2hvd0xvc2VyKCdCJyk7XG4gIH0gZWxzZSB7XG4gICAgLy8gZHJhd1xuICB9XG5cbn1cblxuYmF0dGxlTG9naWMuY29tcHV0ZXJDaG9pY2UgPSBmdW5jdGlvbigpIHtcbiAgdmFyIHJhbmRvbSA9IE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIDMpO1xuICBpZihyYW5kb20gPT09IDApIHtcbiAgICByb2NrLnNldFdlYXBvbigncGxheWVyQicpO1xuICB9IGVsc2UgaWYgKHJhbmRvbSA9PT0gMSkge1xuICAgIHNjaXNzb3JzLnNldFdlYXBvbigncGxheWVyQicpO1xuICB9IGVsc2Uge1xuICAgIHBhcGVyLnNldFdlYXBvbigncGxheWVyQicpO1xuICB9XG4gIHJldHVybiByYW5kb207XG59O1xuXG5iYXR0bGVMb2dpYy5nZXRXaW5uZXIgPSBmdW5jdGlvbihwbGF5ZXIxKSB7XG4gIHBsYXllcjIgPSAgdGhpcy5jb21wdXRlckNob2ljZSgpO1xuICBpZiAocGxheWVyMSA9PT0gcGxheWVyMikgcmV0dXJuICdkcmF3J1xuICAvLyAwID0gcm9jaywgMSA9IHNjaXNzb3IsIDIgPSBwYXBlclxuICBpZihwbGF5ZXIxID09PSAwKSB7XG4gICAgaWYocGxheWVyMiA9PT0gMSkge1xuICAgICAgcmV0dXJuIHRoaXMudXBkYXRlUGxheWVyU2NvcmVzKCdBJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB0aGlzLnVwZGF0ZVBsYXllclNjb3JlcygnQicpO1xuICAgIH1cbiAgfVxuXG4gIGlmKHBsYXllcjEgPT09IDEpIHtcbiAgICBpZihwbGF5ZXIyID09PSAwKSB7XG4gICAgICByZXR1cm4gdGhpcy51cGRhdGVQbGF5ZXJTY29yZXMoJ0InKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHRoaXMudXBkYXRlUGxheWVyU2NvcmVzKCdBJyk7XG4gICAgfVxuICB9XG5cbiAgaWYocGxheWVyMSA9PT0gMikge1xuICAgIGlmKHBsYXllcjIgPT09IDApIHtcbiAgICAgIHJldHVybiB0aGlzLnVwZGF0ZVBsYXllclNjb3JlcygnQScpO1xuICAgIH0gZWxzZSB7XG4gICAgIHJldHVybiB0aGlzLnVwZGF0ZVBsYXllclNjb3JlcygnQicpO1xuICAgIH1cbiAgfSBcbn07XG5cblxubW9kdWxlLmV4cG9ydHMgPSBiYXR0bGVMb2dpYzsiLCJ2YXIgdXRpbHMgPSByZXF1aXJlKCcuL3V0aWxzL2dlbmVyaWMuanMnKTtcbnZhciB1cGRhdGVTY29yZSA9IHJlcXVpcmUoJy4vdXBkYXRlU2NvcmUuanMnKTtcbnZhciBjb250YWluZXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnc2l0ZScpLFxuICAgIHVzZXJOYW1lID0gY29udGFpbmVyLmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ3VzZXJOYW1lJylbMF0sXG4gICAgc3RvcmFnZSA9IHJlcXVpcmUoJy4vdXRpbHMvc3RvcmFnZS5qcycpLFxuICAgIGdhbWVib2FyZCA9IGNvbnRhaW5lci5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdnYW1lYm9hcmQnKVswXSxcbiAgICBwbGF5ZXJBem9uZSA9IGdhbWVib2FyZC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdwbGF5ZXJBem9uZScpWzBdLFxuICAgIHBsYXllckJ6b25lID0gZ2FtZWJvYXJkLmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ3BsYXllckJ6b25lJylbMF0sXG4gICAgcGxheWVyQW5hbWUgPSBwbGF5ZXJBem9uZS5xdWVyeVNlbGVjdG9yQWxsKCcubmFtZSBzcGFuJylbMF0sXG4gICAgcGxheWVyQm5hbWUgPSBwbGF5ZXJCem9uZS5xdWVyeVNlbGVjdG9yQWxsKCcubmFtZSBzcGFuJylbMF0sXG4gICAgcGFnZTEgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgncGFnZTEnKSxcbiAgICBwYWdlMiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdwYWdlMicpLFxuICAgIHBhZ2VzID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgncGFnZScpO1xuXG5cbmluaXRTY3JpcHRzID0ge1xuICBzZXRQYWdlOiBmdW5jdGlvbigpIHtcbiAgICBcbiAgICBmb3IodmFyIGk9MDtpPHBhZ2VzLmxlbmd0aDtpKyspIHtcbiAgICAgIHV0aWxzLnJlbW92ZUNsYXNzKHBhZ2VzW2ldLCAnc2hvdycpO1xuICAgIH1cbiAgICB2YXIgY3VycmVudFBhZ2UgID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Jwcy1jdXJyZW50UGFnZScpIHx8ICdwYWdlMSc7XG4gICAgcGFnZUVsZW1lbnQgID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoY3VycmVudFBhZ2UpO1xuICAgIHV0aWxzLmFkZENsYXNzKHBhZ2VFbGVtZW50LCAnc2hvdycpO1xuXG4gIH0sXG5cblxuICBjaGVja1N0b3JhZ2U6IGZ1bmN0aW9uKCkge1xuICAgICBpZiAoJ2xvY2FsU3RvcmFnZScgaW4gd2luZG93ICYmIHdpbmRvd1snbG9jYWxTdG9yYWdlJ10gIT09IG51bGwpIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgYWxlcnQoJ0Nhbm5vdCBzdG9yZSB1c2VyIHByZWZlcmVuY2VzIGFzIHlvdXIgYnJvd3NlciBkbyBub3Qgc3VwcG9ydCBsb2NhbCBzdG9yYWdlJyk7XG4gICAgfVxuICB9LFxuICBzdG9yZU5hbWVzIDogZnVuY3Rpb24ocGxheWVyTmFtZXMpIHtcbiAgICBzdG9yYWdlLmxvY2FsU3RvcmFnZS5zZXRJdGVtKCdycHMtcGxheWVyQScsIHBsYXllck5hbWVzLnBsYXllckEpO1xuICAgIHN0b3JhZ2UubG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3Jwcy1wbGF5ZXJCJywgcGxheWVyTmFtZXMucGxheWVyQik7XG4gICAgdGhpcy5zZXROYW1lcygpO1xuICB9LFxuICBzZXROYW1lcyA6IGZ1bmN0aW9uKCkge1xuICAgIHBsYXllckFuYW1lLnRleHRDb250ZW50ID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Jwcy1wbGF5ZXJBJykgfHwgJ3Vua25vd24nO1xuICAgIHBsYXllckJuYW1lLnRleHRDb250ZW50ID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Jwcy1wbGF5ZXJCJykgfHwgJ3Vua25vd24nO1xuICAgIHRoaXMubmV4dFBhZ2UoKTtcbiAgfSxcbiAgZ2F0aGVyVXNlck5hbWVzOiBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBwbGF5ZXJOYW1lcyA9IHt9O1xuICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgIHVzZXJOYW1lLmFkZEV2ZW50TGlzdGVuZXIoXCJzdWJtaXRcIiwgZnVuY3Rpb24oZXZlbnQpe1xuICAgICAgdmFyIGZvcm1MZW5ndGggPSB1c2VyTmFtZS5lbGVtZW50cy5sZW5ndGg7XG4gICAgICBcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZm9ybUxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHBsYXllck5hbWVzW3VzZXJOYW1lLmVsZW1lbnRzW2ldLm5hbWVdID0gdXNlck5hbWUuZWxlbWVudHNbaV0udmFsdWU7XG4gICAgICB9O1xuICAgICAgX3RoaXMuc3RvcmVOYW1lcyhwbGF5ZXJOYW1lcyk7XG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH0sIGZhbHNlKTtcbiAgICAgIFxuICB9LFxuXG4gIG5leHRQYWdlOiBmdW5jdGlvbigpIHtcbiAgICB1dGlscy5yZW1vdmVDbGFzcyhwYWdlMSwgJ3Nob3cnKTtcbiAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICB1dGlscy5hZGRDbGFzcyhwYWdlMiwgJ3Nob3cnKTtcbiAgICAgfSAsIDU1MCk7XG5cbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgncnBzLWN1cnJlbnRQYWdlJywgJ3BhZ2UyJyk7XG4gICAgXG4gIH0sXG5cbiAgcmVzZXRCdXR0b24gOiBmdW5jdGlvbigpIHtcbiAgICB2YXIgcmVzZXRCdG4gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcucmVzZXQnKVswXTtcbiAgICByZXNldEJ0bi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAgICAgdXBkYXRlU2NvcmUucmVzZXRHYW1lKCk7XG4gICAgICB1cGRhdGVTY29yZS5kcmF3U2NvcmUoKTtcbiAgICB9KTtcbiAgfVxuICBcbn1cbm1vZHVsZS5leHBvcnRzID0gaW5pdFNjcmlwdHM7IiwidmFyIFdlYXBvbiA9IHJlcXVpcmUoJy4vd2VhcG9uLmpzJyk7XG52YXIgUGFwZXIgPSBPYmplY3QuY3JlYXRlKFdlYXBvbik7XG5cblBhcGVyLnR5cGUgPSAncGFwZXInO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFBhcGVyOyIsInZhciBXZWFwb24gPSByZXF1aXJlKCcuL3dlYXBvbi5qcycpO1xudmFyIFJvY2sgPSBPYmplY3QuY3JlYXRlKFdlYXBvbik7XG5cbi8vIFJvY2suX19wcm90b19fID0gV2VhcG9uO1xuUm9jay50eXBlID0gJ3JvY2snO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFJvY2s7IiwidmFyIFdlYXBvbiA9IHJlcXVpcmUoJy4vd2VhcG9uLmpzJyk7XG52YXIgU2Npc3NvcnMgPSBPYmplY3QuY3JlYXRlKFdlYXBvbik7XG5cblNjaXNzb3JzLnR5cGUgPSAnc2Npc3NvcnMnO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFNjaXNzb3JzOyIsInZhciBzdG9yYWdlID0gcmVxdWlyZSgnLi91dGlscy9zdG9yYWdlLmpzJyksXG4gICAgdXRpbHMgPSByZXF1aXJlKCcuL3V0aWxzL2dlbmVyaWMuanMnKSxcbiAgICB1cGRhdGVTY29yZSA9IHt9LFxuICAgIHBsYXllckFfc2NvcmVfem9uZSAgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcucGxheWVyQXpvbmUgLnNjb3JlJylbMF0sXG4gICAgcGxheWVyQl9zY29yZV96b25lID0gIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5wbGF5ZXJCem9uZSAuc2NvcmUnKVswXSxcbiAgICBwbGF5ZXJBc2NvcmUgPSBzdG9yYWdlLmxvY2FsU3RvcmFnZS5nZXRJdGVtKCdycHMtcGxheWVyQXNjb3JlJykgfHwgMCxcbiAgICBwbGF5ZXJCc2NvcmUgPSBzdG9yYWdlLmxvY2FsU3RvcmFnZS5nZXRJdGVtKCdycHMtcGxheWVyQnNjb3JlJykgfHwgMCxcbiAgICBwYWdlcyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ3BhZ2UnKSxcbiAgICB3ZWFwb25zID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLndlYXBvbicpLFxuICAgIHBsYXllckFXZWFwb24gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnd2VhcG9uQScpLFxuICAgIHBsYXllckJXZWFwb24gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnd2VhcG9uQicpO1xuICAgIFxuXG52YXIgaW5pdGlhbGl6ZSA9IHJlcXVpcmUoJy4vaW5pdGlhbGl6ZS5qcycpO1xuXG51cGRhdGVTY29yZS5kcmF3U2NvcmUgPSBmdW5jdGlvbigpIHtcbiAgcGxheWVyQV9zY29yZV96b25lLnRleHRDb250ZW50ID0gcGxheWVyQXNjb3JlO1xuICBwbGF5ZXJCX3Njb3JlX3pvbmUudGV4dENvbnRlbnQgPSBwbGF5ZXJCc2NvcmU7XG59O1xuXG51cGRhdGVTY29yZS5yZXNldFdlYXBvbnMgPSBmdW5jdGlvbigpIHtcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCB3ZWFwb25zLmxlbmd0aDsgaSsrKSB7XG4gICAgdXRpbHMubmV3Q2xhc3Mod2VhcG9uc1tpXSwgJ3dlYXBvbicpXG4gIH1cbn07XG5cbnVwZGF0ZVNjb3JlLm5ld1Njb3JlID0gZnVuY3Rpb24ocGxheWVyKSB7XG4gIGlmKHBsYXllciA9PT0gJ3BsYXllckEnKSB7XG4gICAgdmFyIHBsYXllckFzY29yZSA9IHN0b3JhZ2UubG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Jwcy1wbGF5ZXJBc2NvcmUnKTtcbiAgICBwbGF5ZXJBc2NvcmUrKztcbiAgICBzdG9yYWdlLmxvY2FsU3RvcmFnZS5zZXRJdGVtKCdycHMtcGxheWVyQXNjb3JlJywgcGxheWVyQXNjb3JlKTtcbiAgICBwbGF5ZXJBX3Njb3JlX3pvbmUudGV4dENvbnRlbnQgPSBwbGF5ZXJBc2NvcmU7XG4gIH0gZWxzZSBpZiAocGxheWVyID09PSAncGxheWVyQicpIHtcbiAgICB2YXIgcGxheWVyQnNjb3JlID0gc3RvcmFnZS5sb2NhbFN0b3JhZ2UuZ2V0SXRlbSgncnBzLXBsYXllckJzY29yZScpO1xuICAgIHBsYXllckJzY29yZSsrO1xuICAgIHN0b3JhZ2UubG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3Jwcy1wbGF5ZXJCc2NvcmUnLCBwbGF5ZXJCc2NvcmUpO1xuICAgIHBsYXllckJfc2NvcmVfem9uZS50ZXh0Q29udGVudCA9IHBsYXllckJzY29yZTtcbiAgfVxufTtcblxudXBkYXRlU2NvcmUuc2hvd0xvc2VyID0gZnVuY3Rpb24ocGxheWVyKSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgd2VhcG9ucy5sZW5ndGg7IGkrKykge1xuICAgICAgIHV0aWxzLnJlbW92ZUNsYXNzKHdlYXBvbnNbaV0sICdncmF5c2NhbGUnKTtcbiAgIH07XG5cbiAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7IFxuICAgIGlmKHBsYXllciA9PT0gJ0EnKSB7XG4gICAgICB1dGlscy5hZGRDbGFzcyhwbGF5ZXJCV2VhcG9uLCAnZ3JheXNjYWxlJyk7XG4gICAgfSBlbHNlIGlmIChwbGF5ZXIgPT09ICdCJykge1xuICAgICAgdXRpbHMuYWRkQ2xhc3MocGxheWVyQVdlYXBvbiwgJ2dyYXlzY2FsZScpO1xuICAgIH1cbiAgfSwgMzAwKVxuICBcbn07XG5cbnVwZGF0ZVNjb3JlLnNldFBhZ2UgPSBmdW5jdGlvbigpIHtcbiAgICBcbiAgICBmb3IodmFyIGk9MDtpPHBhZ2VzLmxlbmd0aDtpKyspe1xuICAgICAgdXRpbHMucmVtb3ZlQ2xhc3MocGFnZXNbaV0sICdzaG93Jyk7XG4gICAgfVxuICAgIHZhciBjdXJyZW50UGFnZSAgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgncnBzLWN1cnJlbnRQYWdlJykgfHwgJ3BhZ2UxJztcbiAgICBwYWdlRWxlbWVudCAgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChjdXJyZW50UGFnZSk7XG4gICAgdXRpbHMuYWRkQ2xhc3MocGFnZUVsZW1lbnQsICdzaG93Jyk7XG4gIH1cblxudXBkYXRlU2NvcmUucmVzZXRHYW1lID0gZnVuY3Rpb24oKSB7XG4gIGNvbnNvbGUubG9nKCdyZXNldCcpXG4gIHN0b3JhZ2UubG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3Jwcy1wbGF5ZXJBc2NvcmUnLCAwKTtcbiAgc3RvcmFnZS5sb2NhbFN0b3JhZ2Uuc2V0SXRlbSgncnBzLXBsYXllckJzY29yZScsIDApO1xuICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgncnBzLWN1cnJlbnRQYWdlJywgJ3BhZ2UxJyk7XG4gIHRoaXMuZHJhd1Njb3JlKCk7XG4gIHRoaXMucmVzZXRXZWFwb25zKCk7XG4gIHRoaXMuc2V0UGFnZSgpO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSB1cGRhdGVTY29yZTsiLCJ2YXIgdXRpbHMgPSB7fTtcblxudXRpbHMudG9nZ2xlQ2xhc3MgPSBmdW5jdGlvbihlbCwgY2xhc3NOYW1lKSB7XG4gIGlmIChlbC5jbGFzc0xpc3QpIHtcbiAgICBlbC5jbGFzc0xpc3QudG9nZ2xlKGNsYXNzTmFtZSk7XG4gIH0gZWxzZSB7XG4gIHZhciBjbGFzc2VzID0gZWwuY2xhc3NOYW1lLnNwbGl0KCcgJyk7XG4gIHZhciBleGlzdGluZ0luZGV4ID0gY2xhc3Nlcy5pbmRleE9mKGNsYXNzTmFtZSk7XG5cbiAgaWYgKGV4aXN0aW5nSW5kZXggPj0gMClcbiAgICBjbGFzc2VzLnNwbGljZShleGlzdGluZ0luZGV4LCAxKTtcbiAgZWxzZVxuICAgIGNsYXNzZXMucHVzaChjbGFzc05hbWUpO1xuXG4gIGVsLmNsYXNzTmFtZSA9IGNsYXNzZXMuam9pbignICcpO1xuICB9XG59O1xuXG51dGlscy5uZXdDbGFzcyA9IGZ1bmN0aW9uKGVsLCBjbGFzc05hbWUpIHtcbiAgZWwuY2xhc3NOYW1lID0gY2xhc3NOYW1lO1xufTtcblxudXRpbHMuYWRkQ2xhc3MgPSBmdW5jdGlvbihlbCwgY2xhc3NOYW1lKSB7XG4gIGlmIChlbC5jbGFzc0xpc3QpXG4gIGVsLmNsYXNzTGlzdC5hZGQoY2xhc3NOYW1lKTtcbiAgZWxzZVxuICAgIGVsLmNsYXNzTmFtZSArPSAnICcgKyBjbGFzc05hbWU7XG59O1xuXG51dGlscy5yZW1vdmVDbGFzcyA9IGZ1bmN0aW9uKGVsLCBjbGFzc05hbWUpIHtcbiAgaWYgKGVsLmNsYXNzTGlzdClcbiAgZWwuY2xhc3NMaXN0LnJlbW92ZShjbGFzc05hbWUpO1xuZWxzZVxuICBlbC5jbGFzc05hbWUgPSBlbC5jbGFzc05hbWUucmVwbGFjZShuZXcgUmVnRXhwKCcoXnxcXFxcYiknICsgY2xhc3NOYW1lLnNwbGl0KCcgJykuam9pbignfCcpICsgJyhcXFxcYnwkKScsICdnaScpLCAnICcpO1xufTtcblxudXRpbHMuZmFkZUluU2hvdyA9IGZ1bmN0aW9uKGVsKSB7XG4gIGVsLnN0eWxlLm9wYWNpdHkgPSAwO1xuICB2YXIgbGFzdCA9ICtuZXcgRGF0ZSgpO1xuICB2YXIgdGljayA9IGZ1bmN0aW9uKCkge1xuICAgIGVsLnN0eWxlLm9wYWNpdHkgPSArZWwuc3R5bGUub3BhY2l0eSArIChuZXcgRGF0ZSgpIC0gbGFzdCkgLyA0MDA7XG4gICAgbGFzdCA9ICtuZXcgRGF0ZSgpO1xuICAgIGlmICgrZWwuc3R5bGUub3BhY2l0eSA8IDEpIHtcbiAgICAgICh3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lICYmIHJlcXVlc3RBbmltYXRpb25GcmFtZSh0aWNrKSkgfHwgc2V0VGltZW91dCh0aWNrLCAxNilcbiAgICB9XG4gIH07XG5cbiAgdGljaygpO1xufTtcblxuXG5cbm1vZHVsZS5leHBvcnRzID0gdXRpbHM7IiwidmFyIHN0b3JhZ2UgPSB7XG4gIGxvY2FsU3RvcmFnZSA6IHdpbmRvdy5sb2NhbFN0b3JhZ2UsXG4gIHNlc3Npb25TdG9yYWdlIDogIHdpbmRvdy5zZXNzaW9uU3RvcmFnZVxufVxubW9kdWxlLmV4cG9ydHMgPSBzdG9yYWdlOyIsInZhciB1dGlscyA9IHJlcXVpcmUoJy4vdXRpbHMvZ2VuZXJpYy5qcycpO1xudmFyIGNvbnRhaW5lciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdzaXRlJyk7XG52YXIgdXNlck5hbWUgPSBjb250YWluZXIuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgndXNlck5hbWUnKVswXTtcblxuXG52YXIgV2VhcG9uID0ge307XG5cbldlYXBvbi5zZXRWYWx1ZSA9IGZ1bmN0aW9uKCkge1xuICAgIHZhciB0eXBlID0gdGhpcy50eXBlO1xuICAgIHN3aXRjaCAodHlwZSkge1xuICAgICAgICBjYXNlICdyb2NrJyA6XG4gICAgICAgICAgdGhpcy53ZWFwb25WYWx1ZSA9IDBcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnc2Npc3NvcnMnIDpcbiAgICAgICAgICB0aGlzLndlYXBvblZhbHVlID0gMVxuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdwYXBlcicgOlxuICAgICAgICAgIHRoaXMud2VhcG9uVmFsdWUgPSAyXG4gICAgICAgICAgYnJlYWs7XG4gICAgfVxufTtcblxuV2VhcG9uLnNldFdlYXBvbiA9IGZ1bmN0aW9uKHBsYXllcikge1xuICB0aGlzLnBsYXllciA9IHBsYXllcjtcbiAgdmFyIHBsYXllclpvbmUgPSB0aGlzLnBsYXllciArICd6b25lJztcbiAgdmFyIHBsYXllckVsID0gY29udGFpbmVyLmdldEVsZW1lbnRzQnlDbGFzc05hbWUocGxheWVyWm9uZSlbMF07XG4gIHZhciBwbGF5ZXJXZWFwb25FbCA9IHBsYXllckVsLmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ3dlYXBvbicpWzBdO1xuXG4gIHV0aWxzLm5ld0NsYXNzKHBsYXllcldlYXBvbkVsLCAnd2VhcG9uICcgKyB0aGlzLnR5cGUpO1xuICB0aGlzLnNldFZhbHVlKCk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gV2VhcG9uOyJdfQ==
