var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    browserify = require('browserify')
    source = require('vinyl-source-stream'),
    watchify = require('watchify'),
    livereload = require('gulp-livereload'),
    connect = require('gulp-connect'),
    gutil = require('gulp-util'),
    sourceFile = './app/scripts/main.js',
    outputFolder = './app/scripts/',
    outputFileName = 'build.js';


gulp.task('browserify', function() {
    return browserify(sourceFile)
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('./dist/'))
        .pipe(connect.reload());
});

 gulp.task('sass', function() {
    gulp.src('./app/scss/**/*.scss')

      .pipe(plugins.plumber()) 
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.sass())
      .pipe(plugins.sourcemaps.write())
      .pipe(gulp.dest('./app/css'))
      .pipe(connect.reload());
  });

 gulp.task('javascript', function() {
  gulp.src("./dist/**/*.js")
  .pipe(connect.reload());
 });

 gulp.task('connect', function() {
  connect.server({
    root: './',
    livereload: true,
    port: 8000
  });
});



gulp.task('watch', function() {
  gulp.watch("./app/scss/**/*.scss", ["sass"]);
  gulp.watch("./dist/**/*.js", ["javascript"]);

  watchify.args.debug = true;
  var bundler = watchify(browserify(sourceFile, watchify.args));


  bundler.on('update', rebundle)

  function rebundle() {
    return bundler.bundle()
      .on('error', gutil.log.bind(gutil, 'Browserify Error'))
      .pipe(source('bundle.js'))
      .pipe(gulp.dest('./dist'));
  }

  return rebundle();
});

gulp.task('default', ['connect', 'watch']);