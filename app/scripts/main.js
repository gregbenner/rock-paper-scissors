var initialize = require('./initialize.js'),
    battleLogic = require('./battleLogic.js'),
    updateScore = require('./updateScore.js'),
    utils = require('./utils/generic.js'),
    rock = require('./rock.js'),
    paper = require('./paper.js'),
    scissors = require('./scissors.js'),
    storage;

var container = document.getElementById('site'),
    userName = container.getElementsByClassName('userName')[0],
    scoreBoard = container.getElementsByClassName('scoreboard')[0];

if(initialize.checkStorage()) {
   storage = require('./utils/storage.js');
}

initialize.setPage();
initialize.setNames();
updateScore.drawScore();

var weaponChoice = container.getElementsByClassName('weaponChoice');

 var handleWeapon = function() {
    var attribute = this.getAttribute("data-value");

    switch(attribute) {
      case 'rock' :
        rock.setWeapon('playerA');
        weaponValue = rock.weaponValue;
        break;
      case 'scissors' :
        scissors.setWeapon('playerA');
        weaponValue = scissors.weaponValue;
        break;
      case 'paper' :
        paper.setWeapon('playerA');
        weaponValue = paper.weaponValue;
        break;
    }
    battleLogic.getWinner(weaponValue);
};

for(var i=0;i<weaponChoice.length;i++){
  weaponChoice[i].addEventListener('click', handleWeapon, false);
}

initialize.gatherUserNames();
initialize.resetButton();

