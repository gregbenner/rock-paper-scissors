var utils = {};

utils.toggleClass = function(el, className) {
  if (el.classList) {
    el.classList.toggle(className);
  } else {
  var classes = el.className.split(' ');
  var existingIndex = classes.indexOf(className);

  if (existingIndex >= 0)
    classes.splice(existingIndex, 1);
  else
    classes.push(className);

  el.className = classes.join(' ');
  }
};

utils.newClass = function(el, className) {
  el.className = className;
};

utils.addClass = function(el, className) {
  if (el.classList)
  el.classList.add(className);
  else
    el.className += ' ' + className;
};

utils.removeClass = function(el, className) {
  if (el.classList)
  el.classList.remove(className);
else
  el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
};

utils.fadeInShow = function(el) {
  el.style.opacity = 0;
  var last = +new Date();
  var tick = function() {
    el.style.opacity = +el.style.opacity + (new Date() - last) / 400;
    last = +new Date();
    if (+el.style.opacity < 1) {
      (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16)
    }
  };

  tick();
};



module.exports = utils;