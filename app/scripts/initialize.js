var utils = require('./utils/generic.js');
var updateScore = require('./updateScore.js');
var container = document.getElementById('site'),
    userName = container.getElementsByClassName('userName')[0],
    storage = require('./utils/storage.js'),
    gameboard = container.getElementsByClassName('gameboard')[0],
    playerAzone = gameboard.getElementsByClassName('playerAzone')[0],
    playerBzone = gameboard.getElementsByClassName('playerBzone')[0],
    playerAname = playerAzone.querySelectorAll('.name span')[0],
    playerBname = playerBzone.querySelectorAll('.name span')[0],
    page1 = document.getElementById('page1'),
    page2 = document.getElementById('page2'),
    pages = document.getElementsByClassName('page');


initScripts = {
  setPage: function() {
    
    for(var i=0;i<pages.length;i++) {
      utils.removeClass(pages[i], 'show');
    }
    var currentPage  = localStorage.getItem('rps-currentPage') || 'page1';
    pageElement  = document.getElementById(currentPage);
    utils.addClass(pageElement, 'show');

  },


  checkStorage: function() {
     if ('localStorage' in window && window['localStorage'] !== null) {
        return true;
    } else {
        alert('Cannot store user preferences as your browser do not support local storage');
    }
  },
  storeNames : function(playerNames) {
    storage.localStorage.setItem('rps-playerA', playerNames.playerA);
    storage.localStorage.setItem('rps-playerB', playerNames.playerB);
    this.setNames();
  },
  setNames : function() {
    playerAname.textContent = localStorage.getItem('rps-playerA') || 'unknown';
    playerBname.textContent = localStorage.getItem('rps-playerB') || 'unknown';
    this.nextPage();
  },
  gatherUserNames: function() {
      var playerNames = {};
      var _this = this;
      userName.addEventListener("submit", function(event){
      var formLength = userName.elements.length;
      
      for (var i = 0; i < formLength; i++) {
        playerNames[userName.elements[i].name] = userName.elements[i].value;
      };
      _this.storeNames(playerNames);
      event.preventDefault();
    }, false);
      
  },

  nextPage: function() {
    utils.removeClass(page1, 'show');
    setTimeout(function() {
        utils.addClass(page2, 'show');
     } , 550);

    localStorage.setItem('rps-currentPage', 'page2');
    
  },

  resetButton : function() {
    var resetBtn = document.querySelectorAll('.reset')[0];
    resetBtn.addEventListener('click', function() {
      updateScore.resetGame();
      updateScore.drawScore();
    });
  }
  
}
module.exports = initScripts;