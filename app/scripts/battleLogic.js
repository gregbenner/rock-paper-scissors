var updateScore = require('./updateScore.js');
var rock = require('./rock.js');
var paper = require('./paper.js');
var scissors = require('./scissors.js');

var battleLogic = {};


battleLogic.updatePlayerScores = function(winner) {
  if(winner === 'A') {
    updateScore.newScore('playerA');
    updateScore.showLoser('A');
  } else if (winner === 'B') {
    updateScore.newScore('playerB');
    updateScore.showLoser('B');
  } else {
    // draw
  }

}

battleLogic.computerChoice = function() {
  var random = Math.floor(Math.random() * 3);
  if(random === 0) {
    rock.setWeapon('playerB');
  } else if (random === 1) {
    scissors.setWeapon('playerB');
  } else {
    paper.setWeapon('playerB');
  }
  return random;
};

battleLogic.getWinner = function(player1) {
  player2 =  this.computerChoice();
  if (player1 === player2) return 'draw'
  // 0 = rock, 1 = scissor, 2 = paper
  if(player1 === 0) {
    if(player2 === 1) {
      return this.updatePlayerScores('A');
    } else {
      return this.updatePlayerScores('B');
    }
  }

  if(player1 === 1) {
    if(player2 === 0) {
      return this.updatePlayerScores('B');
    } else {
      return this.updatePlayerScores('A');
    }
  }

  if(player1 === 2) {
    if(player2 === 0) {
      return this.updatePlayerScores('A');
    } else {
     return this.updatePlayerScores('B');
    }
  } 
};


module.exports = battleLogic;