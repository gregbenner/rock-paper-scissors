var utils = require('./utils/generic.js');
var container = document.getElementById('site');
var userName = container.getElementsByClassName('userName')[0];


var Weapon = {};

Weapon.setValue = function() {
    var type = this.type;
    switch (type) {
        case 'rock' :
          this.weaponValue = 0
          break;
        case 'scissors' :
          this.weaponValue = 1
          break;
        case 'paper' :
          this.weaponValue = 2
          break;
    }
};

Weapon.setWeapon = function(player) {
  this.player = player;
  var playerZone = this.player + 'zone';
  var playerEl = container.getElementsByClassName(playerZone)[0];
  var playerWeaponEl = playerEl.getElementsByClassName('weapon')[0];

  utils.newClass(playerWeaponEl, 'weapon ' + this.type);
  this.setValue();
}

module.exports = Weapon;