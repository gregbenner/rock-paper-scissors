var Weapon = require('./weapon.js');
var Scissors = Object.create(Weapon);

Scissors.type = 'scissors';

module.exports = Scissors;