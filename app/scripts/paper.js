var Weapon = require('./weapon.js');
var Paper = Object.create(Weapon);

Paper.type = 'paper';

module.exports = Paper;