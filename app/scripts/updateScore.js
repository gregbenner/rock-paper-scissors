var storage = require('./utils/storage.js'),
    utils = require('./utils/generic.js'),
    updateScore = {},
    playerA_score_zone  = document.querySelectorAll('.playerAzone .score')[0],
    playerB_score_zone =  document.querySelectorAll('.playerBzone .score')[0],
    playerAscore = storage.localStorage.getItem('rps-playerAscore') || 0,
    playerBscore = storage.localStorage.getItem('rps-playerBscore') || 0,
    pages = document.getElementsByClassName('page'),
    weapons = document.querySelectorAll('.weapon'),
    playerAWeapon = document.getElementById('weaponA'),
    playerBWeapon = document.getElementById('weaponB');
    

var initialize = require('./initialize.js');

updateScore.drawScore = function() {
  playerA_score_zone.textContent = playerAscore;
  playerB_score_zone.textContent = playerBscore;
};

updateScore.resetWeapons = function() {
  for (var i = 0; i < weapons.length; i++) {
    utils.newClass(weapons[i], 'weapon')
  }
};

updateScore.newScore = function(player) {
  if(player === 'playerA') {
    var playerAscore = storage.localStorage.getItem('rps-playerAscore');
    playerAscore++;
    storage.localStorage.setItem('rps-playerAscore', playerAscore);
    playerA_score_zone.textContent = playerAscore;
  } else if (player === 'playerB') {
    var playerBscore = storage.localStorage.getItem('rps-playerBscore');
    playerBscore++;
    storage.localStorage.setItem('rps-playerBscore', playerBscore);
    playerB_score_zone.textContent = playerBscore;
  }
};

updateScore.showLoser = function(player) {
  for (var i = 0; i < weapons.length; i++) {
       utils.removeClass(weapons[i], 'grayscale');
   };

   setTimeout(function() { 
    if(player === 'A') {
      utils.addClass(playerBWeapon, 'grayscale');
    } else if (player === 'B') {
      utils.addClass(playerAWeapon, 'grayscale');
    }
  }, 300)
  
};

updateScore.setPage = function() {
    
    for(var i=0;i<pages.length;i++){
      utils.removeClass(pages[i], 'show');
    }
    var currentPage  = localStorage.getItem('rps-currentPage') || 'page1';
    pageElement  = document.getElementById(currentPage);
    utils.addClass(pageElement, 'show');
  }

updateScore.resetGame = function() {
  console.log('reset')
  storage.localStorage.setItem('rps-playerAscore', 0);
  storage.localStorage.setItem('rps-playerBscore', 0);
  localStorage.setItem('rps-currentPage', 'page1');
  this.drawScore();
  this.resetWeapons();
  this.setPage();
};

module.exports = updateScore;